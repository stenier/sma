package main

import (
	"flag"
	"fmt"
	"github.com/rwcarlsen/goexif/exif"
	magic "go-magic"
	"image"
	_ "image/jpeg"
	"mime"
	"os"
	"path/filepath"
	"strings"
)

var racine = flag.String("dossier", ".", "dossier contenant les .chk")

//récupère la date de prise de la photo dans les données exif
func getDate(path string) (string, error) {
	fname := path
	f, err := os.Open(fname)
	if err != nil {
		return "", err
	}
	x, err := exif.Decode(f)
	f.Close()
	if err != nil {
		return "", err
	}
	date, err := x.Get("DateTimeOriginal")
	if err != nil {
		return "", err
	}
	return date.StringVal(), nil

}

//retour true si l'image peut être chargée, false si corrompue
func checkImageCorruption(path string) bool {
	// Open the file.
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Decode the image.
	_, t, err := image.Decode(file)
	if err != nil {
		return false
	} else {
		fmt.Println("image non corrompue de type ", t)
		return true
	}
	panic("inatteignable")
}

func renommerVideo(path string) {
	//création du dossier de sortie
	var perm os.FileMode = 0776
	dossier := filepath.Dir(path) + string(os.PathSeparator) + "Videos"
	err := os.Mkdir(dossier, os.ModeDir|perm)
	if err != nil {
		if !os.IsExist(err) {
			panic(err)
		}
	}
	//renommage
	base := strings.TrimRight(filepath.Base(path), filepath.Ext(path))
	nouveau := dossier + string(os.PathSeparator) + base + ".mp4"
	fmt.Println("renomage de ", path, " en ", nouveau)
	err = os.Rename(path, nouveau)
	if err != nil {
		panic(err)
	}

}

func renommerImage(path string, date string) {
	var perm os.FileMode = 0776
	var dossier string
	if ok := checkImageCorruption(path); ok {
		dossier = filepath.Dir(path) + string(os.PathSeparator) + "Photos"
	} else {

		dossier = filepath.Dir(path) + string(os.PathSeparator) + "ImagesCorrompues"
	}
	//création du dossier Photos
	err := os.Mkdir(dossier, os.ModeDir|perm)
	if err != nil {
		if !os.IsExist(err) {
			panic(err)
		}
	}
	nouveau := dossier + string(os.PathSeparator) + strings.Replace(date, " ", "_", -1) + ".JPG"
	fmt.Println("renomage de ", path, " en ", nouveau)
	err = os.Rename(path, nouveau)
	if err != nil {
		panic(err)
	}

}

func makeWalk(m *magic.Db) filepath.WalkFunc {
	//à la base, retourne une fonction pour réutiliser m
	//au final ne sert à rien, m n'étant pas réutilisable
	return func(path string, info os.FileInfo, err error) error {
		//si on a un répertoire, rien à faire
		if info.IsDir() {
			fmt.Print("repetoire : ", path)
			return nil
		}
		//on ne traite que les .CHK
		if filepath.Ext(path) != ".CHK" {
			fmt.Println("path n'est pas un .CHK, rien à faire")
			return nil
		}
		//détermination du type de fichier
		m, err := magic.New(magic.MIME)
		nom, err := m.File(path)
		if err != nil {
			fmt.Println("erreur file : ", err)
			return err
		} else {
			t, _, err := mime.ParseMediaType(nom)
			if err != nil {
				fmt.Println("impossible de parser le type ", nom)
			} else {
				//action selon le type
				switch t {
				case "video/mp4":
					renommerVideo(path)

				case "image/jpeg":
					date, err := getDate(path)
					if err != nil {
						date = strings.TrimRight(filepath.Base(path), filepath.Ext(path))
					}
					renommerImage(path, date)

				default:
					fmt.Println(path, "(", t, ") nest pas un format géré")
				}
			}
			return nil
		}
		panic("impossible")

	}
}

func main() {
	flag.Parse()
	dos, err := filepath.Abs(*racine)
	if err != nil {
		panic("dossier fourni non analysable")
	}
	fmt.Println("récupération des .CHK du dossier ", dos)
	m, err := magic.New(magic.MIME)
	if err != nil {
		fmt.Println("erreur creation magic : ", err)

	} else {
		err = filepath.Walk(dos, makeWalk(m))
		if err != nil {
			fmt.Println("impossible de parcourir ", racine)
		}
	}
}
